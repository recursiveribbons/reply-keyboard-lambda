package dev.robinsyl;

import org.telegram.telegrambots.meta.api.objects.replykeyboard.ReplyKeyboard;

public class Main {
    public static void main(String[] args) {
        ReplyKeyboard replyKeyboard = KeyboardBuilder.createReply()
                .addRow(row -> row
                        .addButton("button 1")
                        .addButton("button 2"))
                .addRow(row -> row
                        .addButton("button 3"))
                .build();

        ReplyKeyboard inlineKeyboard = KeyboardBuilder.createInline()
                .addRow(row -> row
                        .addButton("button 1", "btn 1")
                        .addButton("button 2", "btn 2"))
                .addRow(row -> row
                        .addButton("button 3", "btn 3"))
                .build();
    }
}
